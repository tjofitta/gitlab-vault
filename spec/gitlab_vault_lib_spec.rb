require_relative 'spec_helper'
require_relative '../libraries/vault.rb'

require 'chef-vault/test_fixtures'

describe "GitLab vault insanity" do
  include ChefVault::TestFixtures.rspec_shared_context

  # we need this since in a recipe the context of `node` is Chef::Node
  before do
    allow_any_instance_of(Hash).to receive(:chef_environment).and_return("_default")
    #allow(GitLab::Vault).to receive(:cookbook_name).and_return('my-cookbook')
    #allow(GitLab::Vault).to receive(:recipe_name).and_return('my-recipe')
	end

  it "returns the nested value if there's no chef_vault" do
    node = {'insanity' => {'why' => 'no one knows'}, 'fqdn' => 'my.pretty.node'}
    expect(GitLab::Vault.get(node, 'insanity')).to eq(node['insanity'])
  end

  it "finally starts making some sense, even though we don't like it" do
    # This test has a strong dependency with the json file
    # test/integration/data_bags/vault_name/vault_item_name.json
    # because that's where we are defining the content of the vault item
    #
    # chef_vault points to vault_name that is the fixture folder in which we store the secrets json file
    # this is the actual vault in real chef
    #
    # chef_vault_item is the key that will be loaded from the vault itself to be deeply merged into
    # the node_attributes
    node = {'insanity' => {
      'why' => 'no one knows', 
      'chef_vault' => 'vault_name', 
      'chef_vault_item' => 'vault_item_name' },
      'fqdn' => 'my.pretty.node'
    }
    expect(GitLab::Vault.get(node, 'insanity')).to eq( {
        "why"=>"no one knows", 
        "chef_vault"=>"vault_name",
        "chef_vault_item"=>"vault_item_name", 
        "my_secret_sanity"=>"not really"
      })
  end

  it "can load secrets only from a node" do
    node = {'insanity' => {
      'why' => 'no one knows', 
      'chef_vault' => 'vault_name', 
      'chef_vault_item' => 'vault_item_name' },
      'fqdn' => 'my.pretty.node'
    }
    expect(GitLab::Vault.get_node_secrets_from_node(node, 'insanity')).to eq( {
        "my_secret_sanity"=>"not really"
      })
  end
end
